package id.ac.ub.restapi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    PerpustakaanService perpustakaanService = RetrofitClient.getClient().create(PerpustakaanService.class);
    Buku bukubaru= new Buku();
    EditText judul;
    EditText Id;
    EditText deskripsi;
    Button post;
    Button List;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        judul=findViewById(R.id.judul);
        Id=findViewById(R.id.idbuku);
        deskripsi=findViewById(R.id.deskripsi);
        post=findViewById(R.id.buttonpost);
        List=findViewById(R.id.buttonlist);
        bukubaru.setJudul(judul.getText().toString());
        bukubaru.setId(Id.getText().toString());
        bukubaru.setDeskripsi(deskripsi.getText().toString());

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(bukubaru.getJudul()!=null && bukubaru.getDeskripsi()!=null && bukubaru.getId()!=null){
                    SendPost(bukubaru);
                }
                else {
                    Toast.makeText(MainActivity.this, "please fill out the entire form", Toast.LENGTH_SHORT).show();
                }
            }
        });

        List.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        });
    }

    public void SendPost(Buku buku){
        perpustakaanService.PostBuku(buku).enqueue(new Callback<Buku>() {
            @Override
            public void onResponse(Call<Buku> call, Response<Buku> response) {
                if(response.isSuccessful()){
                    Toast.makeText(MainActivity.this, "success", Toast.LENGTH_SHORT).show();
                    Log.d("success", "list " + response.body().toString());
                }
            }

            @Override
            public void onFailure(Call<Buku> call, Throwable t) {
                Log.d("DataModel", "" + t.getMessage());
                Toast.makeText(getApplicationContext(), "Error : " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}